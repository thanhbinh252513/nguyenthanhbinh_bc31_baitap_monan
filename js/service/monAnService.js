// quản lý API thông qua 1 thằng gọi lại object


const BASE_URL = "https://62b0787be460b79df0469c09.mockapi.io/mon-an";

// export: 
export let monAnService = {
    // tách hàm, gọi API lên
    layDanhSachMonAn: () => {
        // return lại cục axios
        return axios({
            url: BASE_URL,
            method: "GET",
        })
    },
    // api xóa món ăn
    xoaMonAn: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        })
    },
    themMoiMonAn: (monAn) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: monAn,
            // thêm mới thì sẽ có trường data
        });
    },
    layThongTinChiTietMonAn: (idMonAn) => {
        return axios({
            url: `${BASE_URL}/${idMonAn}`,
            method: "GET",
        })
    },
    capNhatMonAn: (monAn) => {
        return axios({
            url: `${BASE_URL}/${monAn.id}`,
            method: "PUT",
            data: monAn,
        })
    }
};