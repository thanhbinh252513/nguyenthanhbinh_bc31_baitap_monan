let danhSachMonAn = [];
let idMonAnDuocChinhSua = null;

// import
import { monAnService } from "./service/monAnService.js"
import { spinnerService } from "./service/spinnerService.js";
import { monAnController } from "./controller/monAnController.js";

// link mockapi
// https://62b0787be460b79df0469c09.mockapi.io/


// XÓA MÓN ĂN
let xoaMonAn = (maMonAn) => {
    spinnerService.batLoading();
    monAnService
        .xoaMonAn(maMonAn)
        .then((res) => {
            spinnerService.tatLoading();

            // rederDanhSachService() chạy lần 2 sau khi xóa món ăn thành công
            rederDanhSachService(res.data);
        })
        .catch((err) => {
            spinnerService.tatLoading();
        });
};
window.xoaMonAn = xoaMonAn;
// *gắn xóa món ăn vào đôi tượng window, window là đối tượng có sẵn

// SỬA MÓN ĂN
let layChiTietMonAn = (idMonAn) => {
    // gán id của món ăn được chọn vào biến idMonAnDuocChinhSua
    idMonAnDuocChinhSua = idMonAn;
    spinnerService.batLoading();
    monAnService
        .layThongTinChiTietMonAn(idMonAn)
        .then((res) => {
            spinnerService.tatLoading();
            // lấy dữ liệu show lên giao diện (binding dữ liệu)
            monAnController.showThongTinLenForm(res.data)
        })
        .catch((err) => {
            spinnerService.tatLoading();
        });
};
window.layChiTietMonAn = layChiTietMonAn;


// tạo renderTable để show danh sách món ăn ra ngoài màn hình
let renderTable = (list) => {
    let contentHTML = "";
    for (let index = 0; index < list.length; index++) {

        let monAn = list[index];
        let contentTr = `<tr>
        <td>${monAn.id}</td>
        <td>${monAn.name}</td>
        <td>${monAn.price}</td>
        <td>${monAn.description}</td>
        <td>
        <button class="btn btn-primary" onclick="layChiTietMonAn(${monAn.id})">Sửa</button>
        <button onclick="xoaMonAn(${monAn.id})" class="btn btn-warning">Xoá</button>
        </td>
        </tr>`
        contentHTML = contentHTML + contentTr;
    }
    document.getElementById("tbody_food").innerHTML = contentHTML;
}



// *vì hàm được sử dụng nhiều nơi nên dùng pp tách hàm
let rederDanhSachService = () => {
    // tự động import gõ key export ( nhớ kiểm tra xem có đuôi js hay không)
    spinnerService.batLoading();
    monAnService
        .layDanhSachMonAn()
        .then((res) => {
            spinnerService.tatLoading();
            danhSachMonAn = res.data;
            renderTable(danhSachMonAn);
        })
        .catch((err) => {
            spinnerService.tatLoading();
        });
};
// * chạy lần đầu khi load lại trang
rederDanhSachService();

// THÊM MỚI MÓN ĂN
let themMonAn = () => {
    // sử dụng món ăn controller
    let monAn = monAnController.layThongTinTuForm();
    monAnService
        .themMoiMonAn(monAn)
        .then((res) => {
            alert("thành công");
            // sau khi thành công gọi lại hàm rederDanhSachService 
            rederDanhSachService();
        })
        .catch((err) => {
            alert("thất bại");
        });

};
window.themMonAn = themMonAn;

// CẬP NHẬT MÓN ĂN
let capNhatMonAn = () => {
    spinnerService.batLoading();
    let monAn = monAnController.layThongTinTuForm();
    let newMonAn = {...monAn, id: idMonAnDuocChinhSua };
    monAnService
        .capNhatMonAn(newMonAn)
        .then((res) => {
            spinnerService.tatLoading();
            // clear thông tin sau khi update thành công
            monAnController.showThongTinLenForm({
                name: "",
                price: "",
                description: "",
            });
            rederDanhSachService();
        })
        .catch((err) => {
            spinnerService.tatLoading();

        });
}
window.capNhatMonAn = capNhatMonAn;