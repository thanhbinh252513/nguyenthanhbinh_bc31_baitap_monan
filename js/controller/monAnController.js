export let monAnController = {
    layThongTinTuForm: () => {
        let tenMonAn = document.getElementById("tenMonAn").value;
        let giaMonAn = document.getElementById("giaMonAn").value;
        let moTaMonAn = document.getElementById("moTaMonAn").value;
        let monAn = {
            name: tenMonAn,
            price: giaMonAn,
            description: moTaMonAn,
        };
        return monAn;
    },

    showThongTinLenForm: (monAn) => {
        document.getElementById("tenMonAn").value = monAn.name;
        document.getElementById("giaMonAn").value = monAn.price;
        document.getElementById("moTaMonAn").value = monAn.description;
    }
};